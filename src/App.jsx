import React from "react";
import Header from "./Header";
import AllBoards from "./AllBoards";
import { Route, Routes } from "react-router-dom";
import AllLists from "./AllLists";

const App = () => {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<AllBoards />} />
        <Route path="/boards/:id" element={<AllLists />} />
      </Routes>
    </>
  );
};

export default App;
