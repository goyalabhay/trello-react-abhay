import React, { useEffect, useState } from "react";
import { TextField } from "@mui/material";
import { key, token } from "./KeyToken";
import axios from "axios";
import CancelIcon from "@mui/icons-material/Cancel";

const CheckItems = ({ checklist, card }) => {
  const [checkItems, setCheckItems] = useState([]);

  async function callCheckItems(checklistId) {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${key}&token=${token}`
      );
      setCheckItems(response.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  async function createCheckItem(checklist, name) {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/checklists/${checklist.id}/checkItems?&key=${key}&token=${token}`,
        { name: name }
      );
      setCheckItems([...checkItems, response.data]);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  function handleKeyDown(e) {
    if (e.key === "Enter") {
      const name = e.target.value;
      createCheckItem(checklist, name);
      return;
    }
  }

  async function deleteCheckItems(checklist, checkItem) {
    try {
      const response = await axios.delete(
        `https://api.trello.com/1/checklists/${checklist.id}/checkItems/${checkItem.id}?key=${key}&token=${token}`
      );
      setCheckItems(
        checkItems.filter((eachcheckItem) => eachcheckItem.id !== checkItem.id)
      );
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  async function updateCheckItems(checkitem, card, value) {
    try {
      const response = await axios.put(
        `https://api.trello.com/1/cards/${card.id}/checkItem/${checkitem.id}?key=${key}&token=${token}`,
        {
          state: value === "incomplete" ? "complete" : "incomplete",
        }
      );
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  useEffect(() => {
    if (checklist) {
      callCheckItems(checklist.id);
    }
  }, [checklist]);

  return (
    <div>
      <div>
        {checkItems.map((checkItem) => (
          <div className="checkItemscontainer">
            <div className="text-cancel" key={checkItem.id}>
              <input
                type="checkbox"
                value={checkItem.name}
                defaultChecked={checkItem.state === "complete" ? true : false}
                onClick={() =>
                  updateCheckItems(checkItem, card, checkItem.state)
                }
              />
              {checkItem.name}
              <CancelIcon
                sx={{ cursor: "pointer" }}
                onClick={() => deleteCheckItems(checklist, checkItem)}
              />
            </div>
          </div>
        ))}
      </div>
      <TextField
        onKeyDown={(e) => handleKeyDown(e)}
        sx={{ marginTop: "10px", backgroundColor: "whitesmoke" }}
        id="filled-hidden-label-small"
        size="small"
        label="Add new checklist item"
        variant="filled"
      />
    </div>
  );
};

export default CheckItems;
