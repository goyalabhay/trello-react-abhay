import React from "react";
import "./index.css";
import { Button, TextField } from "@mui/material";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className="header">
      <Link to={"/"}>
        <Button variant="contained">Home</Button>
      </Link>
      <img className="trelloImg" src="/Trello_logo.png" alt="" />
      <TextField id="outlined-basic" label="Search" variant="outlined" />
    </div>
  );
};

export default Header;
