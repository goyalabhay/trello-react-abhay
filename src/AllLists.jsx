import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Card, TextField } from "@mui/material";
import List from "./List";
import { key, token } from "./KeyToken";

const AllLists = () => {

  const { id } = useParams();
  const [lists, setLists] = useState([]);

  async function callAllLists(setLists, id) {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`
      );
      setLists(response.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  useEffect(() => {
    callAllLists(setLists, id);
  }, []);

  async function createNewList(lists, setLists, name, id) {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/lists?name=${name}&idBoard=${id}&key=${key}&token=${token}`
      );
      setLists([...lists, response.data]);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  function handleKeyDown(e) {
    if (e.key === "Enter") {
      const newName = e.target.value;
      createNewList(lists, setLists, newName, id);
      return;
    }
  }

  return (
    <div className="listFlex">
      {lists.map((list) => (
        <List
          key={list.id}
          id={list.id}
          name={list.name}
          setLists={setLists}
          lists={lists}
          list={list}
        />
      ))}

      <div className="newCard">
        <Card
          variant="outlined"
          sx={{
            width: "200px",
            backgroundColor: "grey",
            color: "white",
            padding: "20px",
            textAlign: "center",
          }}
        >
          <TextField
            onKeyDown={(e) => handleKeyDown(e)}
            sx={{
              display: "flex",
              justifyContent: "center",
              backgroundColor: "white",
            }}
            id="filled-hidden-label-small"
            size="small"
            label="Create New List"
            variant="filled"
          />
        </Card>
      </div>
    </div>
  );
};
export default AllLists;
