import React, { useEffect, useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import { key, token } from "./KeyToken";
import axios from "axios";
import CheckList from "./CheckList";

const Cards = ({ card, cards, setCards }) => {
  async function deleteCard(setCards, cards) {
    try {
      const response = await axios.delete(
        `https://api.trello.com/1/cards/${card.id}?key=${key}&token=${token}`
      );
      const data = response.data;
      setCards(cards.filter((eachCard) => eachCard.id !== card.id));
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  const [open, setOpen] = useState(false);
  const handleClose = () => {
    setOpen(!open);
  };

  return (
    <>
      <div className="card" onClick={() => setOpen(true)}>
        {card.name}{" "}
        <h4>
          {" "}
          <DeleteIcon onClick={() => deleteCard(setCards, cards)} />{" "}
        </h4>
        <CheckList card={card} open={open} handleClose={handleClose} />
      </div>
    </>
  );
};

export default Cards;