import React, { useEffect, useState } from "react";
import axios from "axios";
import { Card, TextField } from "@mui/material";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import StarPurple500SharpIcon from "@mui/icons-material/StarPurple500Sharp";
import { Link } from "react-router-dom";
import { key, token } from "./KeyToken";

const AllBoards = () => {
  const [boards, setBoards] = useState([]);
  useEffect(() => {
    callBoards();
  }, []);

  async function callBoards() {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`
      );
      setBoards(response.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  async function createBoard(name) {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/boards/?name=${name}&key=${key}&token=${token}`
      );
      setBoards([...boards, response.data]);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  function handleFavIcon(id) {
    const updatedBoards = boards.map((board) => {
      if (board.id === id) {
        return { ...board, starred: !board.starred };
      }
      return board;
    });
    setBoards(updatedBoards);
  }

  function handleKeyDown(e) {
    if (e.key === "Enter") {
      const newName = e.target.value;
      createBoard(newName);
      return;
    }
  }

  return (
    <>
      <h1>Starred Boards</h1>
      <div className="mainFlex">
        {boards
          .filter((board) => board.starred)
          .map((board) => (
            <Link to={`/boards/${board.id}`} key={board.id}>
              <div className="flex">
                <Card
                  variant="outlined"
                  sx={{
                    backgroundColor: "rgb(99, 179, 179)",
                    backgroundImage:
                      !board.prefs.backgroundImage === undefined
                        ? "rgb(99, 179, 179)"
                        : `url(${board.prefs.backgroundImage})`,
                    backgroundSize: "cover",
                    height: "180px",
                    width: "360px",
                    display: "flex",
                  }}
                >
                  <div className="boardContents">
                    <div className="titleDiv">
                      <div className="title">{board.name}</div>
                    </div>
                    <div>
                      <StarPurple500SharpIcon
                        onClick={(e) => {
                          handleFavIcon(board.id);
                        }}
                        sx={{ color: "yellow", fontSize: 40 }}
                      />
                    </div>
                  </div>
                </Card>
              </div>
            </Link>
          ))}
      </div>
      <h1>Boards</h1>
      <div className="mainFlex">
        {boards
          .filter((board) => !board.starred)
          .map((board) => (
            <Link to={`/boards/${board.id}`} key={board.id}>
              <div className="flex">
                <Card
                  variant="outlined"
                  sx={{
                    backgroundColor: "rgb(99, 179, 179)",
                    backgroundImage:
                      !board.prefs.backgroundImage === undefined
                        ? "rgb(99, 179, 179)"
                        : `url(${board.prefs.backgroundImage})`,
                    backgroundSize: "cover",
                    height: "180px",
                    width: "360px",
                    display: "flex",
                  }}
                >
                  <div className="boardContents">
                    <div className="titleDiv">
                      <div className="title">{board.name}</div>
                    </div>
                    <div>
                      <StarBorderIcon
                        sx={{ color: "yellow", fontSize: 40 }}
                        onClick={(e) => {
                          handleFavIcon(board.id);
                        }}
                      />
                    </div>
                  </div>
                </Card>
              </div>
            </Link>
          ))}

        <div className="newBoard">
          <Card
            variant="outlined"
            sx={{
              backgroundColor: "black",
              height: "180px",
              width: "360px",
              display: "flex",
            }}
          >
            <TextField
              onKeyDown={(e) => handleKeyDown(e)}
              sx={{
                display: "flex",
                justifyContent: "center",
                backgroundColor: "white",
                height: "30%",
                marginTop: "18%",
                marginLeft: "18%",
              }}
              id="filled-basic"
              label="Create New Board"
              variant="filled"
            />
          </Card>
        </div>
      </div>
    </>
  );
};
export default AllBoards;
