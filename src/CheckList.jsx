import React, { useEffect, useState } from "react";
import { Modal, Box, Typography, TextField } from "@mui/material";
import { key, token } from "./KeyToken";
import axios from "axios";
import DeleteIcon from "@mui/icons-material/Delete";
import CheckListItems from "./CheckItems";

const CheckList = ({ card, open, handleClose }) => {
  const [checklists, setChecklists] = useState([]);

  async function callCheckLists(card) {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/cards/${card.id}/checklists?key=${key}&token=${token}`
      );
      setChecklists(response.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  async function createNewCheckList(card, checklists, name) {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/cards/${card.id}/checklists?key=${key}&token=${token}`,
        { name: name }
      );
      setChecklists([...checklists, response.data]);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  function handleKeyDown(e) {
    if (e.key === "Enter") {
      const name = e.target.value;
      createNewCheckList(card, checklists, name);
      return;
    }
  }

  async function deleteCheckList(card, checklist) {
    try {
      const response = await axios.delete(
        `https://api.trello.com/1/cards/${card.id}/checklists/${checklist.id}?key=${key}&token=${token}`
      );
      const data = response.data;
      setChecklists(
        checklists.filter((eachChecklist) => eachChecklist.id !== checklist.id)
      );
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  useEffect(() => {
    callCheckLists(card);
  }, []);

  return (
    <div>
      <Modal
        style={{
          backgroundColor: "lightgrey",
          width: "50%",
          margin: "auto",
          height: "fit-content",
        }}
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="modal" style={{ height: "100%" }}>
          <div className="box-header">
            <Typography
              id="modal-modal-title"
              variant="h6"
              component="h2"
              sx={{ display: "flex", justifyContent: "center" }}
            >
              {card.name}
            </Typography>
            <div className="checklistFlex">
              {checklists.map((checklist) => (
                <Typography
                  key={checklist.id}
                  sx={{
                    backgroundColor: "whitesmoke",
                    padding: "15px",
                    borderRadius: "5px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <div
                    style={{
                      fontWeight: "bolder",
                      width: "100%",
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    {checklist.name}

                    <DeleteIcon
                      sx={{ cursor: "pointer" }}
                      onClick={() => deleteCheckList(card, checklist)}
                    />
                  </div>
                  <CheckListItems checklist={checklist} card={card} />
                </Typography>
              ))}
            </div>
          </div>

          <div className="box-body">
            <TextField
              onKeyDown={(e) => handleKeyDown(e)}
              sx={{ marginTop: "10px" }}
              id="filled-hidden-label-small"
              size="small"
              label="Add new checklist"
              variant="filled"
            />
          </div>
        </Box>
      </Modal>
    </div>
  );
};

export default CheckList;
