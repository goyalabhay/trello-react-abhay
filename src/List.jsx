import { Card, TextField } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import Cards from "./Cards";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { key, token } from "./KeyToken";

const List = ({ id, name, setLists, lists, list }) => {
  const [cards, setCards] = useState([]);

  async function callCards() {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`
      );
      setCards(response.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  useEffect(() => {
    callCards();
  }, []);

  async function createNewCard(cards, setCards, list, key, token, name) {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/cards?idList=${list.id}&key=${key}&token=${token}`,
        { name: `${name}` }
      );

      setCards([...cards, response.data]);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  function handleKeyDown(e) {
    if (e.key === "Enter") {
      const name = e.target.value;
      createNewCard(cards, setCards, list, key, token, name);
      return;
    }
  }

  async function deleteList(setLists, lists, id) {
    try {
      const response = await axios.put(
        `https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`
      );
      const data = response.data;
      setLists(lists.filter((list) => list.id !== data.id));
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  return (
    <div className="flex" key={id}>
      <Card
        variant="outlined"
        sx={{
          width: "200px",
          backgroundColor: "grey",
          color: "white",
          padding: "20px",
          textAlign: "center",
          height: "fit-content",
        }}
      >
        <div className="listNameFlex">
          <h3>{name}</h3>
          <h3>
            {" "}
            <DeleteIcon onClick={() => deleteList(setLists, lists, id)} />{" "}
          </h3>
        </div>

        <div>
          {cards.map((card) => (
            <Cards
              key={card.id}
              card={card}
              cards={cards}
              setCards={setCards}
            />
          ))}
        </div>

        <div className="addNewCard">
          <TextField
            onKeyDown={(e) => handleKeyDown(e)}
            id="filled-hidden-label-small"
            size="small"
            label="Add new card"
            variant="filled"
          />
        </div>
      </Card>
    </div>
  );
};

export default List;
